from queue import Queue


class StackEmptyException(Exception):
    pass


class SimpleStack(object):
    def __init__(self):
        self.q = Queue()

    def Clear(self):
        self.q.queue.clear()

    def Contains(self, value):
        return value in self.q.queue

    def Peek(self):
        return self.q.queue[0]

    def Push(self, value):
        return self.q.put(value)

    def Pop(self):
        if self.IsEmpty():
            raise StackEmptyException('Can not get item when stack is empty')
        return self.q.get()

    def IsEmpty(self):
        return self.q.empty()
