import unittest

from stack import SimpleStack, StackEmptyException


class TestStack(unittest.TestCase):

    def test_init(self):
        q = SimpleStack()
        self.assertEqual(q.IsEmpty(), True, "Should be True")

    def test_push_and_pop(self):
        value = 1
        q = SimpleStack()
        q.Push(value)
        self.assertEqual(q.Pop(), 1, "Should be 1")

    def test_contains(self):
        value = 1
        q = SimpleStack()
        q.Push(1)
        q.Push(2)

        self.assertEqual(q.Contains(value), True, "Should be True")

    def test_peek(self):
        value = 1
        q = SimpleStack()
        q.Push(1)
        q.Push(2)

        self.assertEqual(q.Peek(), value, "Should be 1")

    def test_clear(self):
        q = SimpleStack()
        q.Push(1)

        self.assertEqual(q.IsEmpty(), False, "Should be False")

        q.Clear()

        self.assertEqual(q.IsEmpty(), True, "Should be True")

    def test_pop_when_stack_empty(self):
        q = SimpleStack()
        try:
            q.Pop()
        except Exception as e:
            self.assertEqual(True, isinstance(e, StackEmptyException), "Should be True")


if __name__ == '__main__':
    unittest.main()
